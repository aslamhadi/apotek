import 'isomorphic-fetch';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

window.baseUrl = 'https://pharmacydjango.herokuapp.com/api';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
