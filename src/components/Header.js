// @flow
import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import ListItemLink from './ListItemLink';

const Header = () => (
  <Navbar fixedTop>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to="/">Apotek</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <ListItemLink to="/kategori">Kategori</ListItemLink>
        <ListItemLink to="/subkategori">Subkategori</ListItemLink>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

export default Header;