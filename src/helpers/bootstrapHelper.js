// @flow
import React from 'react';
import PropTypes from 'prop-types';

import { FormGroup, FormControl, HelpBlock, ControlLabel } from 'react-bootstrap';

export const FieldGroup = (props) => (
  <FormGroup controlId={props.id}>
    <ControlLabel>{props.label}</ControlLabel>
    <FormControl {...props} onChange={props.onChange} />
    {props.help && <HelpBlock>{props.help}</HelpBlock>}
  </FormGroup>
);

FieldGroup.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  help: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};