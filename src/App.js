// @flow
import React from 'react';
import { Grid } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import Header from './components/Header';
import Category from './pages/Category/Category';
import Subcategory from './pages/Subcategory';
import Home from './pages/Home';
import './App.css';

const NoMatch = ({ location }) => (
  <div>
    <h1>404 Page not found</h1>
    <h3>No match for <code>{location.pathname}</code></h3>
  </div>
)

const App = () => (
  <Router>
    <div>
      <Header />
        <Grid>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/kategori" component={Category}/>
            <Route path="/subkategori" component={Subcategory}/>
            <Route component={NoMatch}/>
          </Switch>
        </Grid>
    </div>
  </Router>
);

export default App;