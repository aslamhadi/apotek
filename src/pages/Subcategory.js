// @flow
import React from 'react';
import Header from '../components/Header';

const Subcategory = () => (
  <div>
    <Header />
    <h2>Subcategory</h2>
  </div>
)

export default Subcategory;