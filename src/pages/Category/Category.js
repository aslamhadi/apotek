import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import CategoryForm from './CategoryForm';

class Category extends Component { 
  constructor() {
    super();

    const category = {
      id: 1,
      name: "",
      idx_sale_price: 0,
      idx_sale_price_prescription: 0 
    }

    this.state = {
      categories: [],
      showModal: false,
      category
    };

    this.fetchCategories = this.fetchCategories.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleIdxChange = this.handleIdxChange.bind(this);
    this.handleIdxPrescriptionChange = this.handleIdxPrescriptionChange.bind(this);
  }

  componentDidMount() {
    this.fetchCategories();
  }

  handleNameChange(e: Event) {
    console.log(e.target.value)
    this.setState({
      category:  {
        name: e.target.value 
      }
    })
  }

  handleIdxChange(e: Event) {
    this.setState({
      category:  {
        idx_sale_price: e.target.value 
      }
    })
  }

  handleIdxPrescriptionChange(e: Event) {
    this.setState({
      category:  {
        idx_sale_price_prescription: e.target.value 
      }
    })
  }

  handleSubmit(e: Event) {
    e.preventDefault();
    console.log(e);
  }

  fetchCategories() {
    const url = `${window.baseUrl}/categories/`;
    fetch(url)
    .then((response) => {
      return response.json()
    }).then((json) => {
      this.setState({categories: json});
    }).catch((ex) => {
      console.log('parsing failed', ex)
    })
  }

  render() {
    let modalClose = () => this.setState({ showModal: false });
    const categories = this.state.categories;
    if (categories.length === 0) return (<h2>Loading...</h2>);

    return (
      <div>
        <h2>Kategori</h2>
        <Button bsSize="small" onClick={()=>this.setState({ showModal: true })}>
          Tambah Kategori
        </Button>
        <CategoryForm
          show={this.state.showModal} 
          onHide={modalClose}
          handleSubmit={this.handleSubmit}
          handleIdxChange={this.handleIdxChange}
          handleNameChange={this.handleNameChange}
          handleIdxPrescriptionChange={this.handleIdxPrescriptionChange}
        />

        <BootstrapTable data={categories} striped hover pagination search exportCSV>
          <TableHeaderColumn isKey dataField='id' hidden>Product ID</TableHeaderColumn>
          <TableHeaderColumn dataField='name' dataSort={ true }>Nama</TableHeaderColumn>
          <TableHeaderColumn dataField='idx_sale_price'>Index Penjualan</TableHeaderColumn>
          <TableHeaderColumn dataField='idx_sale_price_prescription'>Index Penjualan Resep</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

export default Category;