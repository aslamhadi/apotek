// @flow
import React from 'react';
import { Button, Modal } from 'react-bootstrap';

import { FieldGroup } from '../../helpers/bootstrapHelper';

const CategoryForm = (props) => {
  return (<Modal show={props.show} onHide={props.onHide}>
  <form onSubmit={props.handleSubmit}>
    <Modal.Header closeButton>
      <Modal.Title>Tambah Kategori</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <FieldGroup
        id="name"
        type="text"
        label="Nama"
        placeholder="Masukkan nama kategori"
        onChange={props.handleNameChange}
      />
      <FieldGroup
        id="idx_sale_price"
        type="text"
        label="Index Penjualan"
        placeholder="Masukkan index penjualan"
        onChange={props.handleIdxChange}
      />
      <FieldGroup
        id="idx_sale_price_prescription"
        type="text"
        label="Index Penjualan Resep"
        placeholder="Masukkan index penjualan resep"
        onChange={props.handleIdxPrescriptionChange}
      />
    </Modal.Body>
    <Modal.Footer>
      <Button onClick={props.onHide}>Tutup</Button>
      <Button bsStyle="primary" type="submit">Submit</Button>
    </Modal.Footer>
  </form>
</Modal>)};

export default CategoryForm;